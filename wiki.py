# Import libraries 
from urllib.request import urljoin
from bs4 import BeautifulSoup
import requests
from urllib.request import urlparse
import re
from collections import deque

inputUrl = "https://en.wikipedia.org/wiki/Special:Random"
links = requests.get(inputUrl)
page = BeautifulSoup(links.text, 'html.parser')
mainBody = page.find(id="bodyContent")

# Only gets wikipedia pages and removes special links
def internal_not_special(href):
	if href:
		if re.compile('^/wiki/').search(href):
			if not re.compile('/\w+:').search(href):
				if not re.compile('#').search(href):
					return True
	return False



# Would be code to get the path to Star Wars, doesn't work.
# def backtrace(parent, start, end):
# 	path = [end]
# 	while path[-1] != start:
# 	    path.append(parent[path[-1]])
# 	path.reverse()
# 	return path



# Using requests to get the links of the popped off link, but it doesn't
def get_url(urlNext):
	u = requests.get(urlNext)
	return u.url

graph = mainBody.find_all('a', href=internal_not_special)
queue = []
visited = []

# Using a BFS to go through the given link, then put them all into a queue
# go through and 
def bfs_visit(visited, graph, node):
    depth = 1
    visited.append(node)
    queue.append(node)
    # For each url within the body
    for url in graph:
    	# If the url isn't in the visited array, then go through, and also make sure we haven't already found star wars.
        if ("https://en.wikipedia.org" + url.get('href')) not in visited and "https://en.wikipedia.org/wiki/Star_Wars" not in queue:
        	# Append the url to the queue
            queue.append("https://en.wikipedia.org" + url.get('href'))
            # Pop off the queue to use for the next part
            newLink = queue.pop()
            getLink = requests.get(newLink)
            pages = BeautifulSoup(getLink.text, 'html.parser')
            pageTitle = pages.find('h1', id="firstHeading").string

            print(pageTitle + " - " + newLink)
            # Supposed to pop the link off the queue, then feed it back through the BFS to then visit the link
            # of the given popped element. However, it just goes back and adds the link to the queue.
            while depth < 5:
                # visitLink = get_url(newLink)
                bfs_visit(visited, graph, newLink)
                # Add to the depth
                depth += 1
                # print(queue)
    # If the star wars site is inside the queue, print that it was found
    if "https://en.wikipedia.org/wiki/Star_Wars" in queue:
        print("Found Star Wars")
        # Would be to used to backtrace and list the jumps to star wars
        # print(backtrace(visited, graph, "https://en.wikipedia.org/wiki/Star_Wars"))
        return
    # If Star Wars was not found, print so
    else:
    	print("Star Wars was not found")

# Call the bfs with the visited list, the body of all the non-special links and the original url
bfs_visit(visited, graph, inputUrl)
